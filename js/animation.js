function setNewAnimation(i, j, newNumber) {
    let theNumber = $('#number-cell-' + i + '-' + j)
    theNumber.css('backgroundColor', getNumberBackgroundColor(newNumber))
    theNumber.css('color', getNumberColor(newNumber))
    theNumber.text(board[i][j])
    theNumber.animate({
        width: '100px',
        height: '100px',
        top: getTop(i),
        left: getLeft(j),
    }, 50)
}

function showMoveAnimation(fromx, fromy, tox, toy) {
    let moveNumberCell = $('#number-cell-' + fromx + '-' + fromy)
    moveNumberCell.animate({
        top: getTop(toy),
        left: getLeft(tox)
    }, 200)
}