$(document).keydown(function (e) {
    switch (e.keyCode) {
        case 37: //左
            if (moveLeft()) {
                getNewNumber();
                isOver() //判断是否满足结束条件
            }
            break;
        case 38: //上
            if (moveUp()) {
                getNewNumber();
                isOver() //判断是否满足结束条件
            }
            break;
        case 39: //右
            if (moveRight()) {
                getNewNumber();
                isOver() //判断是否满足结束条件
            }
            break;
        case 40: //下
            if (moveDown()) {
                getNewNumber();
                isOver() //判断是否满足结束条件
            }
            break;
        default:
            break;
    }
})

// let startx = 0
// let starty = 0
// let endx = 0
// let endy = 0
let startX = 0,
    startY = 0,
    d = -1;
document.documentElement.addEventListener('touchstart', function (e) {
    var touch = e.touches[0]; //获取第一个触点  
    var x = Number(touch.pageX); //页面触点X坐标  
    var y = Number(touch.pageY); //页面触点Y坐标  
    //记录触点初始位置  
    startX = x;
    startY = y;
})

document.addEventListener('touchmove', function (e) {
    e.preventDefault()
}, false);
document.documentElement.addEventListener('touchmove', function (e) {
    var touch = e.touches[0]; //获取第一个触点  
    var x = Number(touch.pageX); //页面触点X坐标  
    var y = Number(touch.pageY); //页面触点Y坐标   

    //判断滑动方向  
    if ((y - startY) > 0 && Math.abs((y - startY) / (x - startX)) > 1) {
        d = 0;
    } else if (y - startY < 0 && Math.abs((y - startY) / (x - startX)) > 1) {
        d = 1;
    } else if ((x - startX) > 0 && Math.abs((y - startY) / (x - startX)) < 1) {
        d = 2;
    } else {
        d = 3;
    }

})
document.documentElement.addEventListener('touchend', function (e) {
    if (d == 2) {
        if (moveRight()) {
            getNewNumber();
            isOver() //判断是否满足结束条件
        }
    } else if (d == 0) {
        if (moveDown()) {
            getNewNumber();
            isOver() //判断是否满足结束条件
        }
    } else if (d == 3) {
        if (moveLeft()) {
            getNewNumber();
            isOver() //判断是否满足结束条件
        }
    } else if (d == 1) {
        if (moveUp()) {
            getNewNumber();
            isOver() //判断是否满足结束条件
        }
    }
})

/* 
var startX = 0,
    startY = 0,
    d = -1;
//touchstart事件  
function touchSatrtFunc(evt) {
    evt.preventDefault(); //阻止触摸时浏览器的缩放、滚动条滚动等  
    var touch = evt.touches[0]; //获取第一个触点  
    var x = Number(touch.pageX); //页面触点X坐标  
    var y = Number(touch.pageY); //页面触点Y坐标  
    //记录触点初始位置  
    startX = x;
    startY = y;
}

//touchmove事件，这个事件无法获取坐标  
function touchMoveFunc(evt) {
    evt.preventDefault(); //阻止触摸时浏览器的缩放、滚动条滚动等  
    var touch = evt.touches[0]; //获取第一个触点  
    var x = Number(touch.pageX); //页面触点X坐标  
    var y = Number(touch.pageY); //页面触点Y坐标   

    //判断滑动方向  
    if ((y - startY) > 0 && Math.abs((y - startY) / (x - startX)) > 1) {
        d = 0;
    } else if (y - startY < 0 && Math.abs((y - startY) / (x - startX)) > 1) {
        d = 1;
    } else if ((x - startX) > 0 && Math.abs((y - startY) / (x - startX)) < 1) {
        d = 2;
    } else {
        d = 3;
    }
}

//touchend事件  
function touchEndFunc(evt) {
    evt.preventDefault(); //阻止触摸时浏览器的缩放、滚动条滚动等  
    if (d == 0) {
        if (moveDown()) {
            getNewNumber();
            isOver() //判断是否满足结束条件
        } //下
    } else if (d == 1) {
        if (moveUp()) {
            getNewNumber();
            isOver() //判断是否满足结束条件
        } //上
    } else if (d == 2) {
        if (moveRight()) {
            getNewNumber();
            isOver() //判断是否满足结束条件
        }
    } else if (d == 3) {
        if (moveLeft()) {
            getNewNumber();
            isOver() //判断是否满足结束条件
        }
    }
}

//绑定事件  
function bindEvent() {
    document.addEventListener('touchstart', touchSatrtFunc, false);
    document.addEventListener('touchmove', touchMoveFunc, false);
    document.addEventListener('touchend', touchEndFunc, false);
}

//判断是否支持触摸事件  

bindEvent(); //绑定事件   */

function moveLeft() {
    //能否向左移动
    if (!canMoveLeft()) {
        return false
    }
    //向左移动
    for (let i = 0; i < 4; i++) {
        for (let j = 1; j < 4; j++) {
            //首先不能为左边第一栏并且不能为空
            if (board[i][j] != 0) {
                for (let k = 0; k < j; k++) {
                    //k是目标移动位置，j是当前
                    //判断目标位置为0并且中间为0
                    if (board[i][k] == 0 && isTransNull(i, k, j, board)) {
                        //开始移动
                        showMoveAnimation(i, j, i, k);
                        board[i][k] = board[i][j];
                        board[i][j] = 0;
                        continue
                        //判断目标位置和当前位置相等并且中间为0
                    } else if (board[i][k] == board[i][j] && isTransNull(i, k, j, board) && !flag[i][k]) {
                        //开始移动
                        showMoveAnimation(i, j, i, k);
                        board[i][k] += board[i][j];
                        board[i][j] = 0;
                        score += board[i][k]
                        //分数累加6a
                        $('#score').text(score)
                        flag[i][k] = true
                        continue
                    }
                }
            }
        }
    }
    //左移之后,重新布局
    setNumberCell()
    return true
}



function moveUp() {
    //能否向上移动
    if (!canMoveUp()) {
        return false
    }
    //向上移动
    for (let i = 0; i < 4; i++) {
        for (let j = 1; j < 4; j++) {
            //首先不能为上边第一栏并且不能为空
            if (board[j][i] != 0) {
                for (let k = 0; k < j; k++) {
                    //k是目标移动位置，j是当前
                    //判断目标位置为0并且中间为0
                    if (board[k][i] == 0 && isRowTransNull(k, j, i, board)) {
                        //开始移动
                        showMoveAnimation(j, i, j, k);
                        board[k][i] = board[j][i];
                        board[j][i] = 0;
                        continue
                        //判断目标位置和当前位置相等并且中间为0
                    } else if (board[k][i] == board[j][i] && isRowTransNull(k, j, i, board) && !flag[k][i]) {
                        //开始移动
                        showMoveAnimation(j, i, j, k);
                        board[k][i] += board[j][i];
                        board[j][i] = 0;
                        score += board[k][i]
                        //分数累加6a
                        $('#score').text(score)
                        flag[k][i] = true
                        continue
                    }
                }
            }
        }
    }
    //左移之后,重新布局
    setNumberCell()
    return true
}

function moveRight() {
    //能否向右移动
    if (!canMoveRight()) {
        return false
    }
    //向右移动
    for (let i = 0; i < 4; i++) {
        for (let j = 2; j >= 0; j--) {
            //首先不能为右边第一栏并且不能为空
            if (board[i][j] != 0) {
                for (let k = 3; k > j; k--) {
                    //k是目标移动位置，j是当前
                    //判断目标位置为0并且中间为0
                    if (board[i][k] == 0 && isTransNull(i, j, k, board)) {
                        //开始移动
                        showMoveAnimation(i, j, i, k);
                        board[i][k] = board[i][j];
                        board[i][j] = 0;
                        continue
                        //判断目标位置和当前位置相等并且中间为0
                    } else if (board[i][k] == board[i][j] && isTransNull(i, j, k, board) && !flag[i][k]) {
                        showMoveAnimation(i, j, i, k);
                        board[i][k] += board[i][j];
                        board[i][j] = 0;
                        score += board[i][k]
                        //分数累加6a
                        $('#score').text(score)
                        flag[i][k] = true
                        continue
                    }
                }
            }
        }
    }
    //左移之后,重新布局
    setNumberCell()
    return true
}

function moveDown() {
    //能否向左移动
    if (!canMoveDown()) {
        return false
    }
    //向下移动
    for (let i = 0; i < 4; i++) {
        for (let j = 2; j >= 0; j--) {
            //首先不能为下边第一栏并且不能为空
            if (board[j][i] != 0) {
                for (let k = 3; k > j; k--) {
                    //k是目标移动位置，j是当前
                    //判断目标位置为0并且中间为0
                    if (board[k][i] == 0 && isRowTransNull(j, k, i, board)) {
                        //开始移动
                        showMoveAnimation(j, i, j, k);
                        board[k][i] = board[j][i];
                        board[j][i] = 0;
                        continue
                        //判断目标位置和当前位置相等并且中间为0
                    } else if (board[k][i] == board[j][i] && isRowTransNull(j, k, i, board) && !flag[k][i]) {
                        //开始移动
                        showMoveAnimation(j, i, j, k);
                        board[k][i] += board[j][i];
                        board[j][i] = 0;
                        score += board[k][i]
                        //分数累加6a
                        $('#score').text(score)
                        flag[k][i] = true
                        continue
                    }
                }
            }
        }
    }
    //左移之后,重新布局
    setNumberCell()
    return true
}

//中间为0函数
function isTransNull(row, col1, col2, board) {
    for (let i = col1 + 1; i < col2; i++) {
        //只要左边不等于0，就返回false
        if (board[row][i] != 0) {
            return false
        }
    }
    return true
}

function isRowTransNull(row1, row2, col, board) {
    for (var i = row1 + 1; i < row2; i++) {
        if (board[i][col] != 0) {
            return false;
        }
    }
    return true;
}

//能否移动函数
function canMoveLeft() {
    for (let i = 0; i < 4; i++) {
        for (let j = 1; j < 4; j++) {
            //判断左边为0，或者左边等于自己,就返回true
            if (board[i][j - 1] == 0 || board[i][j - 1] == board[i][j]) {
                return true
            }
        }
    }
}

function canMoveUp() {
    for (let i = 1; i < 4; i++) {
        for (let j = 0; j < 4; j++) {
            //判断上边为0，或者上边等于自己,就返回true
            if (board[i - 1][j] == 0 || board[i - 1][j] == board[i][j]) {
                return true
            }
        }
    }
}

function canMoveRight() {
    for (let i = 0; i < 4; i++) {
        for (let j = 2; j >= 0; j--) {
            //判断右边为0，或者右边等于自己,就返回true
            if (board[i][j + 1] == 0 || board[i][j + 1] == board[i][j]) {
                return true
            }
        }
    }
}

function canMoveDown() {
    for (let i = 2; i >= 0; i--) {
        for (let j = 0; j < 4; j++) {
            //判断下边为0，或者下边等于自己,就返回true
            if (board[i + 1][j] == 0 || board[i + 1][j] == board[i][j]) {
                return true
            }
        }
    }
}


//判断是否结束函数
function isOver() {
    //没有空的数字格子，并且相邻不相等
    if (!isNull() && canMove()) {
        gameover()
    }
}

//相邻不相等函数
function canMove() {
    if (canMoveDown() || canMoveLeft() || canMoveRight() || canMoveUp()) {
        return false;
    }
    return true;
}

function gameover() {
    $("#grid-container").append("<div id='gameover' class='gameover'><p>你输了！你这个垃圾！</p><span>分数" + score + "</span><a href='javascript:regame();' id='restartgamebutton'>Restart</a></div>");
    var gameover = $("#gameover");
    gameover.css("width", "500px");
    gameover.css("height", "500px");
    gameover.css("background-color", "rgba(0, 0, 0, 0.5)");
}

function regame() {
    $('#gameover').remove()
    newgame()
}