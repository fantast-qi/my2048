let board = []
let score = 0
//防止一下多次计算
let flag = []

$(function () {
    newgame();
})

function newgame() {
    init();
    getNewNumber();
    getNewNumber();
    score = 0
    $('#score').text(score)
}

function init() {
    //生成底层棋盘
    for (let i = 0; i < 4; i++) {
        board[i] = [];
        flag[i] = []
        for (let j = 0; j < 4; j++) {
            board[i][j] = 0
            flag[i][j] = false
            let gridcell = $('#grid-cell-' + i + '-' + j)
            gridcell.css('top', getTop(i))
            gridcell.css('left', getLeft(j))
        }
    }
    //生成数字棋盘，并把原先的数字棋盘抹除
    //每次改变棋盘都要调用一下此方法
    setNumberCell();
}

function setNumberCell() {
    $('.number-cell').remove()
    for (let i = 0; i < 4; i++) {
        for (let j = 0; j < 4; j++) {
            $('#grid-container').append('<div class=number-cell id = number-cell-' + i + '-' + j + '></div>')
            let numberCell = $('#number-cell-' + i + '-' + j)
            if (board[i][j] == 0) {
                numberCell.css('width', 0)
                numberCell.css('height', 0)
                numberCell.css('top', getTop(i) + 50)
                numberCell.css('left', getLeft(j) + 50)
            } else {
                numberCell.css('width', 100)
                numberCell.css('height', 100)
                numberCell.css('top', getTop(i))
                numberCell.css('left', getLeft(j))
                numberCell.css('backgroundColor', getNumberBackgroundColor(board[i][j]))
                numberCell.css('color', getNumberColor(board[i][j]))
                numberCell.text(board[i][j])
            }
            flag[i][j] = false;
        }
    }
}

//判定生成棋子时棋盘有没有满，如果满了就不生成
function isNull() {
    for (let i = 0; i < 4; i++) {
        for (let j = 0; j < 4; j++) {
            if (!board[i][j] == 0) {
                return false
            }
        }
    }
}

function getNewNumber() {
    isNull()

    let randx = parseInt(Math.random() * 4)
    let randy = parseInt(Math.random() * 4)

    while (true) {
        if (board[randx][randy] == 0) {
            break
        }
        randx = parseInt(Math.random() * 4)
        randy = parseInt(Math.random() * 4)
    }

    let newNumber = Math.random() > 0.5 ? 4 : 2
    board[randx][randy] = newNumber

    //数字棋盘生成的动画，每次生成一个新的棋盘都会调用生成动画
    setNewAnimation(randx, randy, newNumber);
}